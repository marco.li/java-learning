import java.util.List;
import java.util.ArrayList;

class Solution {
    public List<List<Integer>> generate(int numRows) {
        if (numRows < 0) {
            numRows = 1;
        }
        List<List<Integer>> ret = new ArrayList<List<Integer>>(numRows);

        ArrayList<Integer> l = null;
        List<Integer> last = null;
        for (int i = 0; i < numRows; i++) {
            l = new ArrayList<Integer>(i + 1);
            if (i > 1) {
                last = ret.get(i - 1);
            }

            for (int j = 0; j < i + 1; j++) {
                if (j == 0) {
                    l.add(1);
                } else if (j == i) {
                    l.add(1);
                } else {
                    l.add(last.get(j - 1) + last.get(j));
                }
            }
            ret.add(l);
        }

        for (int i = 0; i < numRows; i++) {
            List<Integer> L = ret.get(i);
            for (int j = 0; j < i + 1; j++) {
                System.out.print(L.get(j) + "");
            }
            System.out.println("");
        }
        return ret;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 4");
            return;
        }

        Solution solution = new Solution();
        List<List<Integer>> ret = solution.generate(Integer.valueOf(args[0]));
    }
}