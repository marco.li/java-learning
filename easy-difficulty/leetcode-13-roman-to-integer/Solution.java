import java.util.List;
import java.util.ArrayList;

class Solution {
    public int romanToInt(String s) {
        int len = s.length();
        int count = 0;
        s += " ";
        for (int i = 0; i < len; i++) {
            switch (s.charAt(i)) {
                case 'I':
                    if (s.charAt(i + 1) == 'V') {
                        count += 4;
                        i++;
                        // System.out.println("4");
                        continue;
                    } else if (s.charAt(i + 1) == 'X') {
                        count += 9;
                        i++;
                        // System.out.println("9");
                        continue;
                    } else {
                        count += 1;
                        // System.out.println("1");
                        continue;
                    }
                case 'X':
                    if (s.charAt(i + 1) == 'L') {
                        count += 40;
                        i++;
                        // System.out.println("40");
                        continue;
                    } else if (s.charAt(i + 1) == 'C') {
                        count += 90;
                        i++;
                        // System.out.println("90");
                        continue;
                    } else {
                        count += 10;
                        // System.out.println("10");
                        continue;
                    }
                case 'C':
                    if (s.charAt(i + 1) == 'D') {
                        count += 400;
                        i++;
                        // System.out.println("400");
                        continue;
                    } else if (s.charAt(i + 1) == 'M') {
                        count += 900;
                        i++;
                        // System.out.println("900");
                        continue;
                    } else {
                        count += 100;
                        // System.out.println("100");
                        continue;
                    }
                case 'V':
                    count += 5;
                    // System.out.println("5");
                    continue;
                case 'L':
                    count += 50;
                    // System.out.println("50");
                    continue;
                case 'D':
                    count += 500;
                    // System.out.println("500");
                    continue;
                case 'M':
                    count += 1000;
                    // System.out.println("1000");
                    continue;
                default:
            }
        }

        return count;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution MCMXCIV");
            return;
        }

        Solution solution = new Solution();
        int ret = solution.romanToInt(args[0]);
        System.out.println("ret=" + ret);
    }
}