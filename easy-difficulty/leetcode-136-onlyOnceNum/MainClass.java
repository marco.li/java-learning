import java.io.BufferedReader;
import java.io.InputStreamReader;

class Solution {
/*    public int singleNumber(int[] nums) {
        int len = nums.length;
        if (len == 0) {
            return 0;
        }

        int end = len;
        boolean foundEqual = false;
        for (int i = 0; i < end - 1; i++) {
            foundEqual = false;
            // System.out.println("foreach i, i=" + i + ", num=" + nums[i]);
            for (int j = i + 1; j < end; j++) {
                // System.out.println("foreach j, j=" + j + ", num=" + nums[j]);
                if (nums[i] == nums[j]) {
                    nums[j]       = nums[j]^nums[end - 1];
                    nums[end - 1] = nums[j]^nums[end - 1];
                    nums[j]       = nums[j]^nums[end - 1];
                    end--;
                    foundEqual = true;
                    break;
                }
            }
            if (!foundEqual) {
                // System.out.println("ret=" + nums[i]);
                return nums[i];
            }
        }

        // for (int i = 0; i < len; i++) {
        //     System.out.println(nums[i]);
        // }
        return nums[end - 1];
    }*/

    public int singleNumber(int[] nums) {
        int ans = 0;
        for(int num: nums) {
            ans ^= num;
        }
        return ans;
    }
}

public class MainClass {
    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        if (input.length() == 0) {
          return null;
        }
        input = input.substring(1, input.length() - 1);

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }
    
    public static void main(String[] args) throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            int[] nums = stringToIntegerArray(line);
            if (nums == null) {
                break;
            }

            int ret = new Solution().singleNumber(nums);
            String out = String.valueOf(ret);
            System.out.print(out);
        }
    }
}