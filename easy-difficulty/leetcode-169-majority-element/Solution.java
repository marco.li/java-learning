import java.util.Map;
import java.util.HashMap;

class Solution {
    public int majorityElement(int[] nums) {
        float len = nums.length;
        if (len == 0.0) {
            return 0;
        }

        int count = 0;
        Map<Integer, Integer> all = new HashMap<Integer, Integer>();
        for (int num : nums) {
            if (all.get(num) == null) {
                // System.out.println("first num=" + num);
                all.put(num, 1);
            } else {
                all.put(num, all.get(num) + 1);
            }
        }

        Map.Entry<Integer, Integer> ret = null;
        for (Map.Entry<Integer, Integer> entry : all.entrySet()) {
            if (ret == null || entry.getValue() > ret.getValue()) {
                ret = entry;
            }
        }

        return ret.getKey();
    }

    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        if (input.length() == 0) {
          return null;
        }
        input = input.substring(0, input.length());

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            // System.out.println("part=" + part + ", index=" + index);
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 3,4,5,3,4,5,5");
            return;
        }

        Solution solution = new Solution();
        int ret = solution.majorityElement(stringToIntegerArray(args[0]));
        System.out.println("ret=" + ret);
    }
}