import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

class Solution2 {
    public int majorityElement(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        if (input.length() == 0) {
          return null;
        }
        input = input.substring(0, input.length());

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            // System.out.println("part=" + part + ", index=" + index);
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 3,4,5,3,4,5,5");
            return;
        }

        Solution2 solution = new Solution2();
        int ret = solution.majorityElement(stringToIntegerArray(args[0]));
        System.out.println("ret=" + ret);
    }
}