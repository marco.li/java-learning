class Solution {
    public int searchInsert(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            if (target <= nums[i]) {
                return i;
            }
        }
        return nums.length;
    }

    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        if (input.length() == 0) {
          return null;
        }
        input = input.substring(0, input.length());

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for(int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            // System.out.println("part=" + part + ", index=" + index);
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage:  java Solution 4,6,8  7");
            return;
        }

        int[] nums = stringToIntegerArray(args[0]);
        int target = Integer.valueOf(args[1]);
        Solution solution = new Solution();
        int ret = solution.searchInsert(nums, target);
        System.out.println("ret=" + ret);
    }
}