class Solution {
    public String addStrings(String num1, String num2) {
        int len1 = num1.length();
        int len2 = num2.length();
        // StringBuilder sb1 = new StringBuilder(num1);
        // StringBuilder sb2 = new StringBuilder(num2);
        char sb1[] = num1.toCharArray();
        char sb2[] = num2.toCharArray();
        int max = 0;
        if (len1 >= len2) {
            max = len1;
        } else {
            max = len2;
        }

        int count = 0, carry = 0;
        StringBuilder ret = new StringBuilder("");
        for (int i = 0; i < max; i++) {
            int n1 = 0, n2 = 0;
            if (len1-1-i >= 0) {
                n1 = sb1[len1 - 1 - i] - '0';
            }
            if (len2-1-i >= 0) {
                n2 = sb2[len2 - 1 - i] - '0';
            }
            count = n1 + n2 + carry;
            if (count >= 10) {
                count -= 10;
                carry = 1;
            } else {
                carry = 0;
            }
            ret.insert(0, (char)('0' + count));
            // System.out.println("i=" + i + ", ret=" + ret);
        }
        if (carry == 1) {
            ret.insert(0, '1');
        }
        return ret.toString();
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage:  java Solution 345 321");
            return;
        }

        Solution solution = new Solution();
        String ret = solution.addStrings(args[0], args[1]);
        System.out.println("ret=" + ret);
    }
}