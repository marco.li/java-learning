class Solution {
    public boolean isPalindrome(int x) {
        if (x == 0) {
            return true;
        } else if (x < 0 || x % 10 == 0) {
            return false;
        }

        int tmp = x;
        int remainder = 0;
        int reverse = 0;
        while (tmp > 0) {
            remainder = tmp % 10;
            reverse = reverse * 10 + remainder;
            tmp = tmp / 10;
        }

        // System.out.println("x=" + x + ", reverse=" + reverse);
        return x == reverse;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 12321");
            return;
        }

        Solution solution = new Solution();
        boolean ret = solution.isPalindrome(Integer.valueOf(args[0]));
        System.out.println(args[0] + (ret ? " is " : " is not ") + "a palindrome number");
    }
}