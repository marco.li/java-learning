class Parent extends Exception {}

class Child extends Parent {}

class Human {

    public static void main(String[] args) throws Exception {
        try {
            try {
                throw new Child();
            } catch (Parent p) {
                System.out.println("Caught Parent");
                throw p;
            }
        } catch (Child c) {
            System.out.println("Caught Child");
            return;
        }
        finally {
            System.out.println("Finally!");
        }
    }
}