class Parent extends Exception {}

class Child extends Parent {}

class Human {

    public static void main(String[] args) throws Exception {
        try {
            try {
                throw new Parent();
            } catch (Child c) {
                System.out.println("Caught Child");
                throw c;
            }
        } catch (Parent p) {
            System.out.println("Caught Parent");
            return;
        } catch (Exception e) {
            System.out.println("Caught Exception");
            return;
        }
        finally {
            System.out.println("Finally!");
        }
    }
}