package com.example.marcolib;

public class MyClass {
    public MyClass() {
        System.out.println("Hello MyClass");
    }

    public static void main(String[] args) {
        int len = args.length;

        if (len == 0) {
            System.out.println("You are calling MyClass, not argument");
            return;
        }

        for (int i = 0; i < args.length; i++) {
            System.out.println("MyClass main(), " + args[i]);
        }

        MyClass myClass = new MyClass();
    }
}