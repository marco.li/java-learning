# Generate JAR - by AndroidStudio



## Test command

```cmd
$ java -jar ./app/marcolib/build/libs/marcolib.jar
You are calling MyClass, not argument
```

```cmd
$ java -jar ./app/marcolib/build/libs/marcolib.jar 1 2 3 4
MyClass main(), 1
MyClass main(), 2
MyClass main(), 3
MyClass main(), 4
Hello MyClass
```



## Note

Add below command in build.gradle of module, for generating the correct MANIFEST.MF of JAR.

```javas
jar {
    manifest {
        attributes 'Main-Class': 'com.example.marcolib.MyClass'  }
}
```

