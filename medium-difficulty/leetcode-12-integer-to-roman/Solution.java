class Solution {
    public String intToRoman(int num) {
        int[] base = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] str = {
            "M",
            "CM",
            "D",
            "CD",
            "C",
            "XC",
            "L",
            "XL",
            "X",
            "IX",
            "V",
            "IV",
            "I",
        };

        StringBuilder ret = new StringBuilder("");
        for (int i = 0; num > 0; i++) {
            if (num < base[i]) {
                continue;
            }
            int result = num / base[i];
            num %= base[i];
            // System.out.println("result=" + result + ",    num=" + num);
            for (int k = 0; k < result; k++) {
                ret.append(str[i]);
            }
        }
        // System.out.println("OUTPUT: " + ret);
        return ret.toString();
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 345");
            return;
        }

        Solution solution = new Solution();
        String ret = solution.intToRoman(Integer.valueOf(args[0]));
        System.out.println("ret=" + ret);
    }
}