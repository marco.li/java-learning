import java.util.Map;
import java.util.HashMap;

class Solution2 {
    public int lengthOfLongestSubstring(String s) {
        int len = s.length();
        if (len == 0) {
            return 0;
        }

        Map<Character, Integer> map = new HashMap<Character, Integer>();
        int window = 0;
        for (int start = 0, end = 0; end < len; end++) {
            char c = s.charAt(end);
            System.out.println("\n--------start=" + start + ",  end=" + end);
            if (map.containsKey(c)) {
                System.out.println("start=" + start + ", map.get(c)=" + map.get(c));
                start = Math.max(start, map.get(c));
                System.out.println("--contains " + c +",  start=" + start);
            }
            window = Math.max(end - start + 1, window);
                System.out.println("window=" + window);
            map.put(c, end + 1);
        }
        return window;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution abcababcab");
            return;
        }

        Solution2 solution = new Solution2();
        int ret = solution.lengthOfLongestSubstring(args[0]);
        System.out.println("ret=" + ret);
    }
}