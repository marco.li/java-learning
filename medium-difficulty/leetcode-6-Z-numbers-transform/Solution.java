import java.util.List;
import java.util.ArrayList;

class Solution {
    public String convert(String s, int numRows) {
        System.out.println("convert()-" + s + ", " + numRows);
        if(numRows < 2) return s;
        List<StringBuilder> rows = new ArrayList<StringBuilder>();
        for(int i = 0; i < numRows; i++) rows.add(new StringBuilder());
        int i = 0, flag = -1;
        for(char c : s.toCharArray()) {
            rows.get(i).append(c);
            if(i == 0 || i == numRows -1) flag = - flag;
            i += flag;
        }
        StringBuilder res = new StringBuilder();
        for(StringBuilder row : rows) res.append(row);
        return res.toString();
    }

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage:  java Solution ABCDEFG 3");
            return;
        }

        Solution solution = new Solution();
        String ret = solution.convert(args[0], Integer.valueOf(args[1]));
        System.out.println("OUT:  " + ret);
    }
}