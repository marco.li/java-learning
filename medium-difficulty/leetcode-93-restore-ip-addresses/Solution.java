import java.util.List;
import java.util.ArrayList;

class Solution {
    public List<String> restoreIpAddresses(String s) {
        int len = s.length();
        List<String> list = new ArrayList<String>();

        if (len < 4 || len > 12) {
            return list;
        }

        int a = 0, b = 0, c = 0, d = 0;
        for (int i = 1; i <= 3 && i < len - 2; i++) {
            if (s.charAt(0) == '0' && i != 1) {
                // System.out.println("NOOOO1, continue");
                continue;
            }
            for (int j = i + 1; j - i <= 3 && j < len - 1; j++) {
                if (s.charAt(i) == '0' && j - i != 1) {
                    // System.out.println("NOOOO2, continue");
                    continue;
                }
                for (int k = j + 1; k - j <= 3 && k < len; k++) {
                    if ((s.charAt(j) == '0' && k - j != 1)
                    || (s.charAt(k) == '0' && len - k != 1)) {
                        // System.out.println("NOOOO3, continue");
                        continue;
                    }
                    a = Integer.parseInt(s.substring(0, i));
                    if (a > 255) continue;
                    b = Integer.parseInt(s.substring(i, j));
                    if (b > 255) continue;
                    c = Integer.parseInt(s.substring(j, k));
                    if (c > 255) continue;
                    d = Integer.parseInt(s.substring(k, len));
                    if (d > 255) continue;
                    // System.out.println("a=" + a + ", b=" + b + ", c=" + c + ", d=" + d);
                    StringBuilder tmp = new StringBuilder("");
                    tmp.append(a).append('.').append(b).append('.').append(c).append('.').append(d);
                    // String tmp = "";
                    // System.out.println("gettttttttttttttt:" + tmp);
                    list.add(tmp.toString());
                }
            }
        }
        return list;
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:  java Solution 2342132");
            return;
        }

        Solution solution = new Solution();
        List<String> ret = solution.restoreIpAddresses(args[0]);
        System.out.println("ret=" + ret);
    }
}