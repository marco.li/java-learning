Usage:
	java StringReverserThroughStack abcdefg

Output:
	BEFORE:	abcdefg
	AFTER:	gfedcba

