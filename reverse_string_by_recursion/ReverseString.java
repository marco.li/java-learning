class ReverseString
{
    public ReverseString() {}

    public static String reverse(String originStr) {
        System.out.println(originStr);
        if (originStr == null || originStr.length() <= 1)
            return originStr;

        return reverse(originStr.substring(1)) + originStr.charAt(0);
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Please input one string!");
            return;
        }

        String str = ReverseString.reverse(args[0]);

        System.out.println("After reverse: " +str);
    }
}
