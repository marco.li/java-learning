class B extends A
{
    static {
        System.out.println("b");
    }

    public B() {
        super(999);
        System.out.println("B");
    }
}
