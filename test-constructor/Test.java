class Test
{
    public static void main(String[] args) {
        System.out.print("Test.main() ");
        for (int i = 0; i < args.length; i++) {
            System.out.print(i + " " + args[i]);
        }
        System.out.println();

        try {
            Thread.sleep(600);
        } catch (Exception e) {
            System.out.println(e);
        }

        A ab = new B();
        ab = new B();
    }
}
